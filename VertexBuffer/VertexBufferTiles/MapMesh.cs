﻿using System.Diagnostics;
using System.Threading.Tasks;
using SharpDX;
using SharpDX.Direct3D11;
using SharpDX.Mathematics.Interop;
using VertexBufferTiles.Helpers;
using VertexBufferTiles.Map;
using VertexBufferTiles.Rendering;

namespace VertexBufferTiles
{
    public class MapMesh : Mesh<VertexPositionColorTexture>
    {
        private Stopwatch _timer = Stopwatch.StartNew();
        private readonly Map.Map _map;
        private const int VerticesPerQuad = 6;
        private readonly Vector3 _tilePixelSize = new RawVector3(64f, 32f, 0);
        private readonly Vector3 _tileSize = new(32f, 32f, 0f);
        private readonly ITilePositionStrategy _tilePositionStrategy;
        private (Size2 size, ShaderResourceView resource) _terrainTexture = (new Size2(1, 1), null);

        public MapMesh(Map.Map map, Camera camera) : base(camera)
        {
            _map = map;
            _tilePositionStrategy = new IsometricTilePositionStrategy(_map.Size, _tileSize);
        }

        public void Initialize(Device device)
        {
            _terrainTexture = (size: new Size2(800, 800), resource: device.LoadTextureFromFile("Terrain.png"));
            BuildMap();
            SelectionConstant = new SelectionConstant()
            {
                Cursor = new Vector4(0, 0, 0, 0),
                HoverTiles = new RawVector4[4],
                SelectedTiles = new RawVector4[4],
            };
            InitializeShaders(device, "terrain.ps.hlsl", "standard.vs.hlsl", new[] {_terrainTexture});
            InitializeVertexBuffer(Vertices);
        }

        public MapTile[,] BuildMap()
        {
            var vertexCount = (int) (_map.Size.X * _map.Size.Y * VerticesPerQuad);
            var vertices = new VertexPositionColorTexture[vertexCount];
            var tiles = new MapTile[(int) _map.Size.X, (int) _map.Size.Y];
            
            Parallel.For(0, (int) _map.Size.X, x =>
            {
                x = (int) _map.Size.X - x - 1;

                for (var y = (int) _map.Size.Y - 1; y >= 0; y--)
                    tiles[x, y] = SetTileVertices(vertices, x, y, (y % 10, x % 10) switch
                    {
                        (0, _) => new RawVector3(1, 0, 0),
                        (_, 0) => new RawVector3(0, 1, 0),
                        _ => GetTextureTile(x, y)
                    });
            });
            
            Vertices = vertices;
            return tiles;

            RawVector3 GetTextureTile(int x, int y)
            {
                return (x + y % 4) switch
                {
                    0 => new RawVector3(0, 0, 0),
                    1 => new RawVector3(0, 1, 0),
                    2 => new RawVector3(1, 0, 0),
                    _ => new RawVector3(0, 0, 0)
                };
            }
        }

        public MapTile[,] UpdateMap()
        {
            var vertexCount = (int) (_map.Size.X * _map.Size.Y * VerticesPerQuad);
            var vertices = new VertexPositionColorTexture[vertexCount];
            var tiles = new MapTile[(int) _map.Size.X, (int) _map.Size.Y];
            
            Parallel.For(0, (int) _map.Size.X, x =>
            {
                x = (int) _map.Size.X - x;

                for (var y = (int) _map.Size.Y - 1; y >= 0; y--)
                    tiles[x, y] = SetTileVertices(vertices, x, y, (y % 10, x % 10) switch
                    {
                        (0, _) => new RawVector3(1, 0, 0),
                        (_, 0) => new RawVector3(0, 1, 0),
                        _ => GetTextureTile(x, y)
                    });
            });
            
            Vertices = vertices;
            return tiles;

            RawVector3 GetTextureTile(int x, int y)
            {
                return (x + y % 4) switch
                {
                    0 => new RawVector3(0, 0, 0),
                    1 => new RawVector3(0, 1, 0),
                    2 => new RawVector3(1, 0, 0),
                    _ => new RawVector3(0, 0, 0)
                };
            }
        }

        public override void UpdateWorldConstant()
        {
            var timing = new RawVector4(
                (float) Stopwatch.Elapsed.TotalMilliseconds,
                (float) Stopwatch.Elapsed.TotalSeconds,
                (float) Stopwatch.Elapsed.TotalMilliseconds % 1000,
                (float) Stopwatch.Elapsed.TotalSeconds % 1);

            _worldConstantBuffer.UpdateValue(new WorldConstant
            {
                WorldViewProjectProjection = WorldViewProjection,
                Timing = timing,
                MapSize = _map.Size
            });
        }

        private MapTile SetTileVertices(VertexPositionColorTexture[] vertexes, int x, int y, RawVector3 terrainSprite)
        {
            var tilePosition = _tilePositionStrategy.Calculate(x, y);
            var sprite = new Sprite(_terrainTexture, terrainSprite, _tilePixelSize);
            var texturedQuad = new TexturedTileQuad(new Vector3(x, y, 0), tilePosition, _tileSize, sprite);

            var offset = (y + x * (int) _map.Size.Y) * VerticesPerQuad;
            vertexes[offset + 0] = texturedQuad.Vertex1;
            vertexes[offset + 1] = texturedQuad.Vertex2;
            vertexes[offset + 2] = texturedQuad.Vertex3;
            vertexes[offset + 3] = texturedQuad.Vertex4;
            vertexes[offset + 4] = texturedQuad.Vertex5;
            vertexes[offset + 5] = texturedQuad.Vertex6;

            return new MapTile
            {
                Coordinates = new Vector3(x, y, 0),
                Quad = texturedQuad,
                Sprite = sprite
            };
        }

        public override void Draw(DeviceContext deviceContext)
        {
            _map.Mesh.UpdateSelectionBuffer(SelectionConstant);
            
            base.Draw(deviceContext);
        }
    }
}