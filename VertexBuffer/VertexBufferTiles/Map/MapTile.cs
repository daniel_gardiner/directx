using SharpDX;
using SharpDX.Mathematics.Interop;

namespace VertexBufferTiles.Map
{
    public class MapTile
    {
        public TexturedTileQuad Quad { get; set; }
        public Vector3 Coordinates { get; set; }
        public Sprite Sprite { get; set; }
    }
}