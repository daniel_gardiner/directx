using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using SharpDX;
using SharpDX.Mathematics.Interop;
using SharpDX.Windows;
using VertexBufferTiles.Map;
using VertexBufferTiles.Rendering;
using D3D11 = SharpDX.Direct3D11;

namespace VertexBufferTiles
{
    public class Game : IDisposable
    {
        private readonly DX11 _dx11 = new();
        private readonly Camera _camera = new();

        private readonly Stopwatch _stopwatch = Stopwatch.StartNew();
        private double _lastReset;
        private readonly RenderForm _renderForm;

        private int _frameCount;
        private double _measuredFps;
        private readonly RawVector3 _mapSize = new(300, 300, 1);
        private readonly Map.Map _map;
        private readonly Mesh<VertexPositionColorTexture> _mesh;
        private readonly Vector2 _screenSize = new(1000, 600);

        public static float VA1;
        public static float VA2;
        public static float VB1;
        public static float VB2;
        private int _lastHitTest;
        private int _frame;

        public Game()
        {
            _renderForm = new RenderForm("My first SharpDX game");
            _renderForm.ClientSize = new Size((int) _screenSize.X, (int) _screenSize.Y);
            _dx11.Initialize(_camera, new Rendering.Configuration("VertexBuffer", _screenSize.X, _screenSize.Y, false, true), _renderForm.Handle);
            _mesh = new Mesh<VertexPositionColorTexture>(_camera);
            _mesh.InitializeShaders(_dx11.Device, "wireframe.ps.hlsl", "standard.vs.hlsl", null);

            _renderForm.KeyDown += KeyDown();
            _renderForm.MouseMove += (sender, args) => { };
            _renderForm.MouseWheel += (sender, args) => { _camera.Zoom(args.Delta); };
            _renderForm.MouseUp += (sender, args) => { };
            _renderForm.MouseDown += (sender, args) =>
            {
                if (args.Button == MouseButtons.Middle)
                {
                    _camera.ResetScroll();
                    _camera.ResetZoom();
                }
            };

            _renderForm.MouseMove += (sender, args) =>
            {
                var current = _camera.ViewMatrixTransition.Current;
                var worldViewProjection = _map.Mesh.ScalingMatrix * _map.Mesh.PanZoomMatrix * current * _camera.ProjectionMatrix;
                var ray = Ray.GetPickRay(args.X, args.Y, new ViewportF(0, 0, _screenSize.X, _screenSize.Y), worldViewProjection);
                var selectedTiles = new List<RawVector3>();

                CheckHitTest(ref ray, ref selectedTiles);
            };

            _renderForm.ClientSize = new Size((int) _screenSize.X, (int) _screenSize.Y);
            _renderForm.AllowUserResizing = false;

            _map = new Map.Map(_mapSize, _camera);
            _map.Initialize(_dx11.Device);
        }

        private void CheckHitTest(ref Ray ray, ref List<RawVector3> selectedTiles)
        {
            if (_frame == _lastHitTest)
                return;

            var start = _stopwatch.Elapsed.TotalMilliseconds;
            HitTest(ref ray, ref selectedTiles);
            Console.WriteLine($"HitTest time: {_stopwatch.Elapsed.TotalMilliseconds - start:N2} ms");
            UpdateSelectionConstant(selectedTiles);
            Console.WriteLine($"HitTest time ALL: {_stopwatch.Elapsed.TotalMilliseconds - start:N2} ms");
            _lastHitTest = _frame;
        }

        private void UpdateSelectionConstant(List<RawVector3> selectedTiles)
        {
            var constant = _map.Mesh.SelectionConstant;
            constant.SelectedTiles[0] = new RawVector4(0, 0, 0, 0);
            constant.SelectedTiles[1] = new RawVector4(0, 0, 0, 0);
            constant.SelectedTiles[2] = new RawVector4(0, 0, 0, 0);
            constant.SelectedTiles[3] = new RawVector4(0, 0, 0, 0);

            for (var index = 0; index < selectedTiles.Count && index < constant.SelectedTiles.Length; index++)
            {
                var selectedTile = selectedTiles[index];
                constant.SelectedTiles[index] = new RawVector4(selectedTile.X, selectedTile.Y, selectedTile.Z, 1);
            }

            _map.Mesh.SelectionConstant = constant;
        }

        private void HitTest(ref Ray ray, ref List<RawVector3> selectedTiles)
        {
            var boxes = _map.MapTree.GetNearestObjects(new TileBoundingBox
            {
                Coordinates = Vector3.Zero,
                BoundingBox = new BoundingBox(ray.Position - Vector3.One, ray.Position + Vector3.One)
            });

            foreach (var box in boxes)
            {
                var boundingBox = box.BoundingBox;
                if (!Collision.RayIntersectsBox(ref ray, ref boundingBox, out float _))
                    continue;

                var culledCorners = CalculateCullCorners(boundingBox.GetCorners());

                if (!AnyCornersHit(culledCorners, ray))
                    selectedTiles.Add(box.Coordinates);
            }
        }

        private List<Vector3[]> CalculateCullCorners(Vector3[] vectors)
        {
            var (minX, maxX, halfX) = (min: vectors.Min(o => o.X), max: vectors.Max(o => o.X), half: (vectors.Max(o => o.X) + vectors.Min(o => o.X)) / 2f);
            var (minY, maxY, halfY) = (min: vectors.Min(o => o.Y), max: vectors.Max(o => o.Y), half: (vectors.Max(o => o.Y) + vectors.Min(o => o.Y)) / 2f);

            var culledCorners = new List<Vector3[]>
            {
                new Vector3[]
                {
                    new(minX, minY, 0),
                    new(minX, halfY, 0),
                    new(halfX, minY, 0)
                },
                new Vector3[]
                {
                    new(maxX, minY, 0),
                    new(halfX, minY, 0),
                    new(maxX, halfY, 0)
                },
                new Vector3[]
                {
                    new(maxX, maxY, 0),
                    new(maxX, halfY, 0),
                    new(halfX, maxY, 0)
                },
                new Vector3[]
                {
                    new(minX, maxY, 0),
                    new(halfX, maxY, 0),
                    new(minX, halfY, 0)
                }
            };

            /*
            _mesh.InitializeVertexBuffer(culledCorners
                .SelectMany(o => o.ToArray())
                .Select(o => new VertexPositionColorTexture
                {
                    Color = new RawColor4(1, 0, 0, 1),
                    Position = o
                })
                .ToArray());
                */

            return culledCorners;
        }

        private static bool AnyCornersHit(List<Vector3[]> culledCorners, Ray ray)
        {
            foreach (var culledCorner in culledCorners)
                if (Collision.RayIntersectsTriangle(ref ray, ref culledCorner[0], ref culledCorner[1], ref culledCorner[2], out Vector3 _))
                    return true;

            return false;
        }

        private KeyEventHandler KeyDown()
        {
            return (sender, args) =>
            {
                if (args.KeyCode == Keys.Escape)
                {
                    Dispose();
                    Application.Exit();
                }

                const float scrollAmount = 2f;

                var scrollIncrement = 10;
                var shift = (args.Modifiers & Keys.Shift) != 0;

                if ((args.KeyCode, shift) is (Keys.F11, _))
                {
                    _map.Mesh.Wireframe = false;
                }

                if ((args.KeyCode, shift) is (Keys.F12, _))
                {
                    _map.Mesh.Wireframe = true;
                }

                if ((args.KeyCode, shift) is (Keys.Left, false))
                {
                    VA1 -= 0.01f;
                }

                if ((args.KeyCode, shift) is (Keys.Right, false))
                {
                    VA1 += 0.01f;
                }

                if ((args.KeyCode, shift) is (Keys.Up, false))
                {
                    VA2 -= 0.01f;
                }

                if ((args.KeyCode, shift) is (Keys.Down, false))
                {
                    VA2 += 0.01f;
                }

                if ((args.KeyCode, shift) is (Keys.Left, true))
                {
                    VB1 -= 0.01f;
                }

                if ((args.KeyCode, shift) is (Keys.Right, true))
                {
                    VB1 += 0.01f;
                }

                if ((args.KeyCode, shift) is (Keys.Up, true))
                {
                    VB2 -= 0.01f;
                }

                if ((args.KeyCode, shift) is (Keys.Down, true))
                {
                    VB2 += 0.01f;
                }

                if ((args.KeyCode, shift) is (Keys.A, _))
                {
                    _camera.Scroll(-scrollAmount / scrollIncrement, 0);
                }

                if ((args.KeyCode, shift) is (Keys.D, _))
                {
                    _camera.Scroll(scrollAmount / scrollIncrement, 0);
                }

                if ((args.KeyCode, shift) is (Keys.W, _))
                {
                    _camera.Scroll(0, -scrollAmount / scrollIncrement);
                }

                if ((args.KeyCode, shift) is (Keys.S, _))
                {
                    _camera.Scroll(0, scrollAmount / scrollIncrement);
                }
            };
        }

        private static Vector3 ToVector3(RawVector3 rawVector3)
        {
            return new(rawVector3.X, rawVector3.Y, rawVector3.Z);
        }

        public void Run()
        {
            _stopwatch.Restart();
            RenderLoop.Run(_renderForm, RenderCallback);
        }

        private void RenderCallback()
        {
            _frame++;
            Draw();
        }

        /// <summary>
        /// Draw the game.
        /// </summary>
        private void Draw()
        {
            CalculateFPS();
            _dx11.BeginScene();
            _camera.Render();
            _map.Draw(_dx11.DeviceContext);
            //_mesh.Draw(_dx11.DeviceContext);
            _dx11.EndScene();
        }

        private void CalculateFPS()
        {
            _frameCount++;
            var elapsed = _stopwatch.ElapsedMilliseconds;
            if (elapsed - _lastReset >= 1000.0f)
            {
                _measuredFps = _frameCount / (elapsed - _lastReset) * 2000;
                _frameCount = 0;
                _lastReset = elapsed;
            }

            if (_frameCount % 1000 == 0)
            {
                //Console.WriteLine(new {VA1, VA2, VB1, VB2});
                _renderForm.Text = $"{_measuredFps} fps";
            }
        }

        public void Dispose()
        {
            _renderForm?.Dispose();
            _dx11.Dispose();
            _camera.Dispose();
            _map.Dispose();
        }
    }
}