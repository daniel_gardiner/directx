﻿using System;
using SharpDX;
using SharpDX.Direct3D11;

namespace VertexBufferTiles
{
    public class Sprite
    {
        public ShaderResourceView Texture { get; }
        public Vector3 Position { get; }
        public Vector3 Size { get; }
        public Size2 TextureSize { get; set; }

        public Sprite((Size2 size, ShaderResourceView resource) texture, Vector3 position, Vector3 size)
        {
            if (texture.size == Size2.Zero)
                throw new Exception("Texture size cannot be zero");
            
            Texture = texture.resource;
            TextureSize = texture.size;
            Size = size;
            Position = position;
        }
    }
}