﻿using SharpDX;
using SharpDX.Mathematics.Interop;

namespace VertexBufferTiles.Map
{
    public class IsometricTilePositionStrategy : ITilePositionStrategy
    {
        private readonly Vector3 _tilePixelSize;
        private readonly Vector3 _halfTileSize;
        private readonly Vector3 _centerOfMap;

        public Vector3 TileSize { get; }
        public Vector3 MapSize { get; }

        public IsometricTilePositionStrategy(RawVector3 mapDimensions, Vector3 tileSize)
        {
            TileSize = tileSize;
            MapSize = mapDimensions;
            _halfTileSize = tileSize;
            _centerOfMap = CalculateXY((int) (MapSize.X / 2f), (int) (MapSize.Y / 2f));
        }

        public Vector3 Calculate(int x, int y)
        {
            var tileOffset = CalculateXY(x, y);
            return tileOffset - _centerOfMap;
        }

        private RawVector3 CalculateXY(int x, int y)
        {
            return new(
                x * _halfTileSize.X + y * _halfTileSize.Y,
                y * _halfTileSize.Y - x * _halfTileSize.X,
                1);
        }
    }
}