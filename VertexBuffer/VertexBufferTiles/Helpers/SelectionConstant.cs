using System.Runtime.InteropServices;
using SharpDX;
using SharpDX.Mathematics.Interop;

namespace VertexBufferTiles.Helpers
{
    [StructLayout(LayoutKind.Explicit, Size = 144)]
    public struct SelectionConstant
    {
        [FieldOffset(0)]
        public Vector4 Cursor;

        [FieldOffset(16), MarshalAs(UnmanagedType.ByValArray, SizeConst = 4)]
        public RawVector4[] HoverTiles;

        [FieldOffset(80), MarshalAs(UnmanagedType.ByValArray, SizeConst = 4)]
        public RawVector4[] SelectedTiles;
    }
}