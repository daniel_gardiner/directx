﻿using SharpDX;
using SharpDX.Mathematics.Interop;

namespace VertexBufferTiles
{
    public class Quad<TVertex> where TVertex : struct, IPositionColorTexture
    {
        private readonly Vector3 _topRight = Vector3.Right + Vector3.Up;
        private readonly Vector3 _bottomLeft = Vector3.Left + Vector3.Down;
        private readonly Vector3 _bottomRight = Vector3.Right + Vector3.Down;
        private readonly Vector3 _topLeft = Vector3.Left + Vector3.Up;

        public Quad(Vector3 mapLocation, RawVector3 quadPosition, Vector3 quadSize)
        {
            _topRight *= quadSize;
            _bottomLeft *= quadSize;
            _bottomRight *= quadSize;
            _topLeft *= quadSize;

            var white = new RawColor4(1, 1, 1, 1);
            Vertex1 = new TVertex {Tile = mapLocation, Position = quadPosition + _bottomLeft, Color = white};
            Vertex2 = new TVertex {Tile = mapLocation, Position = quadPosition + _topLeft, Color = white};
            Vertex3 = new TVertex {Tile = mapLocation, Position = quadPosition + _topRight, Color = white};
            Vertex4 = new TVertex {Tile = mapLocation, Position = quadPosition + _bottomRight, Color = white};
            Vertex5 = new TVertex {Tile = mapLocation, Position = quadPosition + _bottomLeft, Color = white};
            Vertex6 = new TVertex {Tile = mapLocation, Position = quadPosition + _topRight, Color = white};
        }

        public TVertex Vertex1;
        public TVertex Vertex2;
        public TVertex Vertex3;
        public TVertex Vertex4;
        public TVertex Vertex5;
        public TVertex Vertex6;
    }
}