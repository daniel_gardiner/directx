using System.Collections;
using System.Collections.Generic;
using SharpDX;
using SharpDX.Mathematics.Interop;

namespace VertexBufferTiles.Map
{
    public class TexturedTileQuad : TexturedQuad<VertexPositionColorTexture>
    {
        public TexturedTileQuad(Vector3 mapLocation, Vector3 quadPosition, Vector3 quadSize, Sprite sprite)
            : base(mapLocation, quadPosition, quadSize, sprite)
        {
        }

        public IEnumerable<RawVector3> AllVertices
        {
            get
            {
                yield return Vertex1.Position;
                yield return Vertex2.Position;
                yield return Vertex3.Position;
                yield return Vertex4.Position;
                yield return Vertex5.Position;
                yield return Vertex6.Position;
            }
        }
    }
}