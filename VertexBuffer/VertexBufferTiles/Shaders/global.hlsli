
/////////////
// GLOBALS //
/////////////
cbuffer WorldInfo: register(b0)
{
    matrix world_view_projection;
    vector timing;
    vector map_size;
};

cbuffer TexturInfo : register(b1)
{
    float2 pixel_size;
    float2 texel_size;
}

cbuffer SelectionInfo : register(b2)
{
    float4 cursor;
    float4 hover_tiles[4];
    float4 selected_tiles[4];
};

;

////////////// 
// TYPEDEFS //
//////////////
struct VertexShaderInput
{
    float4 position : SV_POSITION;
    float3 tile : TILEPOSITION;
    float4 color : COLOR;
    float2 uv0 : TEXCOORD0;
    float2 uv1 : TEXCOORD1;
};

struct PixelShaderInput
{
    float4 position : SV_POSITION;
    float3 tile : TILEPOSITION;
    float4 color : COLOR;
    float2 uv0 : TEXCOORD0;
    float2 uv1 : TEXCOORD1;
};
