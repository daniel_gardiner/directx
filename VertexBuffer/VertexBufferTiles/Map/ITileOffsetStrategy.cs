﻿using SharpDX;

namespace VertexBufferTiles.Map
{
    public interface ITilePositionStrategy
    {
        Vector3 TileSize { get; }
        Vector3 MapSize { get; }
        Vector3 Calculate(int x, int y);
    }
}