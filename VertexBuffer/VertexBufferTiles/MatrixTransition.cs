using System;
using System.Diagnostics;
using SharpDX;

namespace VertexBufferTiles
{
    public class MatrixTransition
    {
        private readonly Stopwatch _stopwatch;
        public static MatrixTransition Identity => new(Matrix.Identity, Matrix.Identity, 1);
        private readonly Matrix _target;
        private readonly Matrix _start;
        private readonly float _duration;


        public MatrixTransition(Matrix target, Matrix start, float duration)
        {
            _stopwatch = Stopwatch.StartNew();
            _target = target;
            _start = start;
            _duration = duration;
        }

        public Matrix Current
        {
            get
            {
                var percent = Math.Min(1f, _stopwatch.ElapsedMilliseconds / _duration);
                return Matrix.Lerp(_start, _target, percent);
            }
        }
        
        public Matrix Target => Matrix.Lerp(_start, _target, 1);
    }
}