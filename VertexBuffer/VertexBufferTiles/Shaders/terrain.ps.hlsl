#include "math.hlsli"
#include "global.hlsli"

Texture2D textureMap;
SamplerState sample;

float4 main(PixelShaderInput input) : SV_TARGET
{
    float4 tex0 = textureMap.Sample(sample, input.uv0);
    for (uint i = 0; i < 4; i++)
    {
        if (input.tile.x == 0 && input.tile.y == 0)
            continue;

        if (input.tile.x == selected_tiles[i].x &&
            input.tile.y == selected_tiles[i].y)
        {
            tex0 = tex0.a > 0
                       ? lerp(float4(0, 1, 0, 1), tex0, 0.5)
                       : tex0;
        }
    }

    //if(fmod(input.tile.x, 2) == 0 && fmod(input.tile.y, 2) == 0)
    {
        float4 tex1 = textureMap.Sample(sample, input.uv0 + float2(texel_size * float2(-1.0f, -1.0f)));
        float4 tex2 = textureMap.Sample(sample, input.uv0 + float2(texel_size * float2(-1.0f, 0.0f)));
        float4 tex3 = textureMap.Sample(sample, input.uv0 + float2(texel_size * float2(-1.0f, 1.0f)));
        float4 tex4 = textureMap.Sample(sample, input.uv0 + float2(texel_size * float2(0.0f, -1.0f)));
        float4 tex5 = textureMap.Sample(sample, input.uv0 + float2(texel_size * float2(0.0f, 0.0f)));
        float4 tex6 = textureMap.Sample(sample, input.uv0 + float2(texel_size * float2(0.0f, 1.0f)));
        float4 tex7 = textureMap.Sample(sample, input.uv0 + float2(texel_size * float2(1.0f, -1.0f)));
        float4 tex8 = textureMap.Sample(sample, input.uv0 + float2(texel_size * float2(1.0f, 0.0f)));
        float4 tex9 = textureMap.Sample(sample, input.uv0 + float2(texel_size * float2(1.0f, 1.0f)));


        if (tex1.a == 0 && tex0.a != 0)
            return float4(0.05, 0.05, 0.05, 1);
        if (tex2.a == 0 && tex0.a != 0)
            return float4(0.05, 0.05, 0.05, 1);
        if (tex3.a == 0 && tex0.a != 0)
            return float4(0.05, 0.05, 0.05, 1);
        if (tex4.a == 0 && tex0.a != 0)
            return float4(0.05, 0.05, 0.05, 1);
        if (tex5.a == 0 && tex0.a != 0)
            return float4(0.05, 0.05, 0.05, 1);
        if (tex6.a == 0 && tex0.a != 0)
            return float4(0.05, 0.05, 0.05, 1);
        if (tex7.a == 0 && tex0.a != 0)
            return float4(0.05, 0.05, 0.05, 1);
        if (tex8.a == 0 && tex0.a != 0)
            return float4(0.05, 0.05, 0.05, 1);
        if (tex9.a == 0 && tex0.a != 0)
            return float4(0.05, 0.05, 0.05, 1);
    }
    /*
    float4 textColor = lerp(textureMap.Sample(sample, tex1),
                            lerp(textureMap.Sample(sample, tex2),
                                 lerp(textureMap.Sample(sample, tex3),
                                      lerp(textureMap.Sample(sample, tex4),
                                           lerp(textureMap.Sample(sample, tex5),
                                                lerp(textureMap.Sample(sample, tex6),
                                                     lerp(textureMap.Sample(sample, tex7),
                                                          lerp(textureMap.Sample(sample, tex8), textureMap.Sample(sample, tex9), 0.5f), 0.5f), 0.5f), 0.5f), 0.5f), 0.5f), 0.0f);
    */

    return tex0;
}
