using System.Runtime.InteropServices;
using SharpDX;
using SharpDX.Mathematics.Interop;

namespace VertexBufferTiles
{
    [StructLayout(LayoutKind.Sequential)]
    public struct VertexPositionColorTexture : IPositionColorTexture
    {
        public RawVector3 Position { get; set; }
        public RawVector3 Tile { get; set; }
        public RawColor4 Color { get; set; }
        public RawVector2 UV0 { get; set; }
        public RawVector2 UV1 { get; set; }

        public VertexPositionColorTexture(RawVector3 position)
        {
            Position = position;
            Tile = new RawVector3(0, 0, 0);
            Color = new RawColor4(1, 1, 1, 1);
            UV0 = Vector2.Zero;
            UV1 = Vector2.Zero;
        }
    }
}