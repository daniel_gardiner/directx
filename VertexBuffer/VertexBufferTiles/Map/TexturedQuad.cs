﻿using SharpDX;
using SharpDX.Mathematics.Interop;

namespace VertexBufferTiles.Map
{
    public class TexturedQuad<TVertex> : Quad<TVertex> where TVertex : struct, IPositionColorTexture
    {
        /*
        private readonly Vector2 _bottomLeft = new(0f, 0.5f);
        private readonly Vector2 _topLeft = new(0.5f, 0);
        private readonly Vector2 _topRight = new(1f, 0.5f);
        private readonly Vector2 _bottomRight = new(0.5f, 1f);
        */

        private readonly Vector3 _bottomLeft = new(0f, 1f, 0f);
        private readonly Vector3 _topLeft = new(0f, 0f, 0f);
        private readonly Vector3 _topRight = new(1f, 0.0f, 0f);
        private readonly Vector3 _bottomRight = new(1f, 1f, 0f);

        public TexturedQuad(Vector3 mapLocation, Vector3 quadPosition, Vector3 quadSize, Sprite sprite) : base(mapLocation, quadPosition, quadSize)
        {
            var texelSize = new RawVector3(1f / sprite.TextureSize.Width, 1f / sprite.TextureSize.Height, 0);
            var texelPosition = sprite.Position * texelSize * sprite.Size;

            Vertex1.UV0 = ToVector2(texelPosition + _bottomLeft * texelSize * sprite.Size);
            Vertex2.UV0 = ToVector2(texelPosition + _topLeft * texelSize * sprite.Size);
            Vertex3.UV0 = ToVector2(texelPosition + _topRight * texelSize * sprite.Size);
            Vertex4.UV0 = ToVector2(texelPosition + _bottomRight * texelSize * sprite.Size);
            Vertex5.UV0 = ToVector2(texelPosition + _bottomLeft * texelSize * sprite.Size);
            Vertex6.UV0 = ToVector2(texelPosition + _topRight * texelSize * sprite.Size);

            Vertex1.UV1 = ToVector2(texelPosition + _bottomLeft * texelSize * sprite.Size);
            Vertex2.UV1 = ToVector2(texelPosition + _topLeft * texelSize * sprite.Size);
            Vertex3.UV1 = ToVector2(texelPosition + _topRight * texelSize * sprite.Size);
            Vertex4.UV1 = ToVector2(texelPosition + _bottomRight * texelSize * sprite.Size);
            Vertex5.UV1 = ToVector2(texelPosition + _bottomLeft * texelSize * sprite.Size);
            Vertex6.UV1 = ToVector2(texelPosition + _topRight * texelSize * sprite.Size);
        }

        private RawVector2 ToVector2(Vector3 pos)
        {
            return new Vector2(pos.X, pos.Y);
        }
    }
}