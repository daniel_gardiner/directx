﻿using System;
using System.Windows.Forms;
using SharpDX;
using SharpDX.Mathematics.Interop;

namespace VertexBufferTiles.Rendering
{
    public class Camera : IDisposable
    {
        private int _width;
        private int _height;
        public MatrixTransition PanTransition { get; private set; } = new(Matrix.Identity, Matrix.Identity, 1);
        public MatrixTransition ScaleTransition { get; private set; } = new(Matrix.Identity, Matrix.Identity, 1);
        public float CurrentZoom { get; private set; } = 1;
        public Vector2 CurrentScroll { get; set; }
        public MatrixTransition ViewMatrixTransition { get; set; } = new(Matrix.Identity, Matrix.Identity, 1);
        public Vector3 Position { get; set; }
        private Vector3 Rotation { get; set; }
        public RawMatrix ViewMatrix { get; private set; }
        public Matrix ProjectionMatrix { get; set; }

        public void SetViewport(int width, int height)
        {
            _height = height;
            _width = width;
            Position = Vector3.BackwardLH;
            ProjectionMatrix = Matrix.OrthoLH(_width, _height, -0.1f, 1000f);
        }

        public void SetPosition(Vector3 position)
        {
            Position = position;
        }

        public void Render()
        {
            var cameraTarget = Vector3.Zero; // Looking at the origin 0,0,0
            var cameraUp = Vector3.UnitY; // Y+ is Up
            var viewMatrix = Matrix.LookAtLH(Position, cameraTarget, cameraUp);

            if (ViewMatrixTransition == null)
                UpdateZoomScroll();

            ViewMatrix = viewMatrix;
            ViewMatrix *= ViewMatrixTransition.Current;
        }

        private Matrix CalculateRotation()
        {
            var pitch = Rotation.X * 0.0174532925f;
            var yaw = Rotation.Y * 0.0174532925f;
            var roll = Rotation.Z * 0.0174532925f;
            var rotationMatrix = Matrix.RotationYawPitchRoll(yaw, pitch, roll);
            return rotationMatrix;
        }

        public void Scroll(float x, float y)
        {
            CurrentScroll += new Vector2(x, y);
            UpdateZoomScroll();
        }

        public void Zoom(int delta)
        {
            CurrentZoom -= delta / -600f;
            UpdateZoomScroll();
        }

        public void ZoomTo(float zoom)
        {
            CurrentZoom = zoom;
            UpdateZoomScroll();
        }

        public void ResetZoom()
        {
            CurrentZoom = 1;
            UpdateZoomScroll();
        }

        private void ConstrainZoom()
        {
            CurrentZoom = CurrentZoom switch
            {
                < 0.2f => 0.2f,
                > 17 => 17,
                _ => CurrentZoom
            };
        }

        public void ResetScroll() => CurrentScroll = Vector2.Zero;

        private void UpdateZoomScroll()
        {
            ConstrainZoom();
            ScaleTransition = new MatrixTransition(Matrix.Scaling(CurrentZoom), ScaleTransition.Current, 200);
            PanTransition = new MatrixTransition(Matrix.Translation(-CurrentScroll.X * 100, CurrentScroll.Y * 100, 0), PanTransition.Current, 200);
            ViewMatrixTransition = new MatrixTransition(Matrix.Identity, ViewMatrixTransition.Current, 200);
        }

        public void Dispose()
        {
        }
    }
}