using System;
using System.Diagnostics;
using System.Linq;
using SharpDX;
using SharpDX.Mathematics.Interop;
using UltimateQuadTree;
using VertexBufferTiles.Rendering;
using D3D11 = SharpDX.Direct3D11;

namespace VertexBufferTiles.Map
{
    public struct TileBoundingBox : IEquatable<BoundingBox>, IFormattable
    {
        public TileBoundingBox(RawVector3 coordinates, BoundingBox boundingBox)
        {
            Coordinates = coordinates;
            BoundingBox = boundingBox;
        }

        public Vector3 Coordinates;
        public BoundingBox BoundingBox;

        public bool Equals(BoundingBox other) => BoundingBox.Equals(other);
        public string ToString(string format, IFormatProvider formatProvider) => BoundingBox.ToString(format, formatProvider);
    }

    public class Map
    {
        private readonly Stopwatch _stopwatch = Stopwatch.StartNew();
        public QuadTree<TileBoundingBox> MapTree { get; }
        public RawVector3 Size { get; }
        public MapTile[,] Tiles { get; }

        public Map(RawVector3 size, Camera camera)
        {
            Size = size;
            Mesh = new MapMesh(this, camera);
            Tiles = Mesh.BuildMap();

            var start = _stopwatch.ElapsedMilliseconds;
            var minX = Mesh.Vertices.Min(o => o.Position.X);
            var maxX = Mesh.Vertices.Max(o => o.Position.X);
            var minY = Mesh.Vertices.Min(o => o.Position.Y);
            var maxY = Mesh.Vertices.Max(o => o.Position.Y);

            MapTree = new QuadTree<TileBoundingBox>(minX, minY, maxX - minX, maxY - minY, new MapQuadBounds(), maxLevel: 10);
            foreach (var tile in Tiles)
            {
                var minTileX = tile.Quad.AllVertices.Min(o => o.X);
                var maxTileX = tile.Quad.AllVertices.Max(o => o.X);
                var minTileY = tile.Quad.AllVertices.Min(o => o.Y);
                var maxTileY = tile.Quad.AllVertices.Max(o => o.Y);
                
                var boundingBox = new BoundingBox(new Vector3(minTileX, minTileY, 0), new Vector3(maxTileX, maxTileY, 0));
                MapTree.Insert(new TileBoundingBox(tile.Coordinates, boundingBox));
                if (tile.Coordinates.X + tile.Coordinates.Y % 100 == 0)
                {
                    Console.WriteLine($"tile = {tile.Coordinates} | Elapsed: {_stopwatch.ElapsedMilliseconds - start:N0}ms");
                }
            }
        }

        public MapMesh Mesh { get; }

        public void Initialize(D3D11.Device device)
        {
            Mesh.Initialize(device);
        }

        public void Draw(D3D11.DeviceContext d3dDeviceContext)
        {
            Mesh.Draw(d3dDeviceContext);
        }

        public void Dispose()
        {
            Mesh.Dispose();
        }
    }
}