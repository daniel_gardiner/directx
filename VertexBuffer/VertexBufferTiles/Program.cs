﻿using System;

namespace VertexBufferTiles
{
    internal class Program
    {
        [STAThread]
        private static void Main(string[] args)
        {
            using(var game = new Game())
            {
                game.Run();
            }
        }
    }
}