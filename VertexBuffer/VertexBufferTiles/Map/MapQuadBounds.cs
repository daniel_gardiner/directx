using System.Linq;
using UltimateQuadTree;

namespace VertexBufferTiles.Map
{
    public class MapQuadBounds : IQuadTreeObjectBounds<TileBoundingBox>
    {
        public double GetLeft(TileBoundingBox obj) => obj.BoundingBox.Minimum.X;
        public double GetRight(TileBoundingBox obj) => obj.BoundingBox.Maximum.X;
        public double GetTop(TileBoundingBox obj) => obj.BoundingBox.Minimum.Y;
        public double GetBottom(TileBoundingBox obj) => obj.BoundingBox.Maximum.Y;
    }
}