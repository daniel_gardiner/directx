using System.Runtime.InteropServices;
using SharpDX.Mathematics.Interop;

namespace VertexBufferTiles.Helpers
{
    [StructLayout(LayoutKind.Explicit, Size = 96)]
    public struct WorldConstant
    { 
        [FieldOffset(0)]
        public RawMatrix WorldViewProjectProjection;

        [FieldOffset(64)]
        public RawVector4 Timing;

        [FieldOffset(80)]
        public RawVector3 MapSize;
    }
}