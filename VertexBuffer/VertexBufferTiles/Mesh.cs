using System;
using System.Diagnostics;
using System.Linq;
using SharpDX;
using SharpDX.D3DCompiler;
using SharpDX.Direct3D;
using SharpDX.DXGI;
using SharpDX.Mathematics.Interop;
using VertexBufferTiles.Helpers;
using VertexBufferTiles.Rendering;
using static SharpDX.D3DCompiler.ShaderBytecode;
using D3D11 = SharpDX.Direct3D11;

namespace VertexBufferTiles
{
    public class Mesh<TVertex> : IDisposable where TVertex : struct
    {
        protected readonly Stopwatch Stopwatch = Stopwatch.StartNew();
        private D3D11.InputLayout _inputLayout;
        private ShaderSignature _inputSignature;
        private D3D11.Buffer _vertexBuffer;
        private D3D11.VertexShader _vertexShader;
        private D3D11.PixelShader _pixelShader;
        private D3D11.PixelShader _wireFramePixelShader;
        protected ConstantBuffer<WorldConstant> _worldConstantBuffer;
        private ConstantBuffer<TextureConstant> _textureConstantBuffer;
        private ConstantBuffer<SelectionConstant> _selectionConstantBuffer;
        private D3D11.ShaderResourceView[] _shaderResourceViews;
        private SamplerStateCollection _samplerCollection;
        private D3D11.BlendState _alphaBlendingState;
        private D3D11.Device _device;
        private readonly Camera _camera;
        public bool Wireframe { get; set; }
        public TVertex[] Vertices { get; protected set; }
        public SelectionConstant SelectionConstant { get; set; }
        public PrimitiveTopology Topology { get; set; } = PrimitiveTopology.TriangleList;
        public Matrix ScalingMatrix => Matrix.Scaling(1, 0.5f, 1);
        public Matrix PanZoomMatrix => _camera.PanTransition.Current * _camera.ScaleTransition.Current;
        private TextureConstant _textureConstant;

        private readonly D3D11.InputElement[] _inputElements =
        {
            new("SV_POSITION", 0, Format.R32G32B32_Float, 0, 0, D3D11.InputClassification.PerVertexData, 0),
            new("TILEPOSITION", 0, Format.R32G32B32_Float, D3D11.InputElement.AppendAligned, 0, D3D11.InputClassification.PerVertexData, 0),
            new("COLOR", 0, Format.R32G32B32A32_Float, D3D11.InputElement.AppendAligned, 0, D3D11.InputClassification.PerVertexData, 0),
            new("TEXCOORD", 0, Format.R32G32_Float, D3D11.InputElement.AppendAligned, 0, D3D11.InputClassification.PerVertexData, 0),
            new("TEXCOORD", 1, Format.R32G32_Float, D3D11.InputElement.AppendAligned, 0, D3D11.InputClassification.PerVertexData, 0)
        };

        private D3D11.VertexBufferBinding _vertexBufferBindings;

        protected Matrix WorldViewProjection
        {
            get
            {
                var projectionView = _camera.ProjectionMatrix * _camera.ViewMatrix;
                var worldViewProjectProjection = ScalingMatrix * PanZoomMatrix * projectionView;
                return Matrix.Transpose(worldViewProjectProjection);
            }
        }

        public Mesh(Camera camera)
        {
            _camera = camera;
        }

        public void InitializeShaders(D3D11.Device device, string pixelShader, string vertexShader, (Size2 size, D3D11.ShaderResourceView resource)[] shaderResourceViews)
        {
            _device = device;
            _samplerCollection = new SamplerStateCollection(device);

            using var wireframePixelShaderByteCode = CompileFromFile($"shaders\\wireframe.ps.hlsl", "main", "ps_4_0", ShaderFlags.Debug, EffectFlags.None, null, new FileIncludeHandler());
            using var pixelShaderByteCode = CompileFromFile($"shaders\\{pixelShader}", "main", "ps_4_0", ShaderFlags.Debug, EffectFlags.None, null, new FileIncludeHandler());
            using var vertexShaderByteCode = CompileFromFile($"shaders\\{vertexShader}", "main", "vs_4_0", ShaderFlags.Debug, EffectFlags.None, null, new FileIncludeHandler());
            _vertexShader = new D3D11.VertexShader(device, vertexShaderByteCode);
            _pixelShader = new D3D11.PixelShader(device, pixelShaderByteCode);
            _wireFramePixelShader = new D3D11.PixelShader(device, wireframePixelShaderByteCode);
            _inputSignature = ShaderSignature.GetInputSignature(vertexShaderByteCode);
            _inputLayout = new D3D11.InputLayout(device, _inputSignature, _inputElements);
            _worldConstantBuffer = new ConstantBuffer<WorldConstant>(device);
            _textureConstantBuffer = new ConstantBuffer<TextureConstant>(device);
            _selectionConstantBuffer = new ConstantBuffer<SelectionConstant>(device);
            _shaderResourceViews = shaderResourceViews?.Select(o => o.resource).ToArray() ?? new D3D11.ShaderResourceView[0];

            UpdateTextureBuffer(shaderResourceViews);
        }

        private void UpdateTextureBuffer((Size2 size, D3D11.ShaderResourceView resource)[] shaderResourceViews)
        {
            if (shaderResourceViews == null)
                return;

            var size = shaderResourceViews[0].size;
            _textureConstant = new TextureConstant
            {
                PixelSize = new RawVector2(size.Width, size.Height),
                TexelSize = new RawVector2(1f / size.Width, 1f / size.Height),
            };
            _textureConstantBuffer.UpdateValue(_textureConstant);
        }

        public void InitializeVertexBuffer(TVertex[] vertices)
        {
            Vertices = vertices;
            _vertexBuffer = D3D11.Buffer.Create(
                _device,
                D3D11.BindFlags.VertexBuffer,
                vertices,
                accessFlags: D3D11.CpuAccessFlags.Write,
                usage: D3D11.ResourceUsage.Dynamic);
            _vertexBufferBindings = new D3D11.VertexBufferBinding(_vertexBuffer, Utilities.SizeOf<TVertex>(), 0);
        }

        public void UpdateVertexBuffer(TVertex[] vertices)
        {
            _device.ImmediateContext.MapSubresource(_vertexBuffer, D3D11.MapMode.WriteDiscard, D3D11.MapFlags.None, out var mappedResource);
            mappedResource.WriteRange(vertices);
            _device.ImmediateContext.UnmapSubresource(_vertexBuffer, 0);
        }

        public virtual void UpdateWorldConstant()
        {
            var timing = new RawVector4(
                (float) Stopwatch.Elapsed.TotalMilliseconds,
                (float) Stopwatch.Elapsed.TotalSeconds,
                (float) Stopwatch.Elapsed.TotalMilliseconds % 1000,
                (float) Stopwatch.Elapsed.TotalSeconds % 1);

            _worldConstantBuffer.UpdateValue(new WorldConstant
            {
                WorldViewProjectProjection = WorldViewProjection,
                Timing = timing,
            });
        }

        public void UpdateSelectionBuffer(SelectionConstant selectionConstant)
        {
            SelectionConstant = selectionConstant;
            _selectionConstantBuffer.UpdateValue(new SelectionConstant
            {
                Cursor = selectionConstant.Cursor,
                HoverTiles = selectionConstant.HoverTiles,
                SelectedTiles = selectionConstant.SelectedTiles
            });
        }

        public void SetShaders(D3D11.DeviceContext d3dDeviceContext)
        {
            d3dDeviceContext.VertexShader.Set(_vertexShader);
            d3dDeviceContext.VertexShader.SetConstantBuffer(0, _worldConstantBuffer.Buffer);
            d3dDeviceContext.VertexShader.SetConstantBuffer(1, _textureConstantBuffer.Buffer);
            d3dDeviceContext.VertexShader.SetConstantBuffer(2, _selectionConstantBuffer.Buffer);
            d3dDeviceContext.VertexShader.SetShaderResources(0, _shaderResourceViews);

            d3dDeviceContext.PixelShader.Set(_pixelShader);
            d3dDeviceContext.PixelShader.SetSampler(0, _samplerCollection.PointClamp);
            d3dDeviceContext.PixelShader.SetConstantBuffer(0, _worldConstantBuffer.Buffer);
            d3dDeviceContext.PixelShader.SetConstantBuffer(1, _textureConstantBuffer.Buffer);
            d3dDeviceContext.PixelShader.SetConstantBuffer(2, _selectionConstantBuffer.Buffer);
            d3dDeviceContext.PixelShader.SetShaderResources(0, _shaderResourceViews);
        }

        private void SetVertexBuffer(D3D11.DeviceContext d3dDeviceContext, PrimitiveTopology primitiveTopology)
        {
            d3dDeviceContext.InputAssembler.InputLayout = _inputLayout;
            d3dDeviceContext.InputAssembler.SetVertexBuffers(0, _vertexBufferBindings);
            d3dDeviceContext.InputAssembler.PrimitiveTopology = primitiveTopology;
        }

        public virtual void Draw(D3D11.DeviceContext d3dDeviceContext)
        {
            if (Vertices == null)
                return;
            
            UpdateWorldConstant();
            SetVertexBuffer(d3dDeviceContext, PrimitiveTopology.TriangleList);
            SetShaders(d3dDeviceContext);

            if (Wireframe)
            {
                d3dDeviceContext.PixelShader.Set(_wireFramePixelShader);
                d3dDeviceContext.Rasterizer.State = new D3D11.RasterizerState(_device, new D3D11.RasterizerStateDescription
                {
                    FillMode = D3D11.FillMode.Wireframe,
                    CullMode = D3D11.CullMode.Back,
                    IsMultisampleEnabled = true
                });
            }
            else
            {
                d3dDeviceContext.PixelShader.Set(_pixelShader);
                d3dDeviceContext.Rasterizer.State = new D3D11.RasterizerState(_device, new D3D11.RasterizerStateDescription
                {
                    FillMode = D3D11.FillMode.Solid,
                    CullMode = D3D11.CullMode.Back,
                    IsMultisampleEnabled = true
                });
            }

            var blendStateDesc = new D3D11.BlendStateDescription();
            blendStateDesc.RenderTarget[0].IsBlendEnabled = true;
            blendStateDesc.RenderTarget[0].SourceBlend = D3D11.BlendOption.One;
            blendStateDesc.RenderTarget[0].DestinationBlend = D3D11.BlendOption.InverseSourceAlpha;
            blendStateDesc.RenderTarget[0].BlendOperation = D3D11.BlendOperation.Add;
            blendStateDesc.RenderTarget[0].SourceAlphaBlend = D3D11.BlendOption.One;
            blendStateDesc.RenderTarget[0].DestinationAlphaBlend = D3D11.BlendOption.Zero;
            blendStateDesc.RenderTarget[0].AlphaBlendOperation = D3D11.BlendOperation.Add;
            blendStateDesc.RenderTarget[0].RenderTargetWriteMask = D3D11.ColorWriteMaskFlags.All;

            // Create the blend state using the description.
            _alphaBlendingState = new D3D11.BlendState(d3dDeviceContext.Device, blendStateDesc);
            d3dDeviceContext.OutputMerger.SetBlendState(_alphaBlendingState);
            d3dDeviceContext.Draw(Vertices.Length, 0);
        }

        public void Dispose()
        {
            _vertexBuffer.Dispose();
            _vertexBuffer.Dispose();
            _vertexShader.Dispose();
            _pixelShader.Dispose();
            _inputLayout.Dispose();
            _inputSignature.Dispose();
        }
    }
}