using SharpDX.Direct3D11;

namespace VertexBufferTiles.Helpers
{
    /// <summary>
    /// Sampler state collection.
    /// </summary>
    public sealed class SamplerStateCollection
    {
        /// <summary>
        /// Default state is using linear filtering with texture coordinate clamping.
        /// </summary>
        public readonly SamplerState Default;

        /// <summary>
        /// Point filtering with texture coordinate wrapping.
        /// </summary>
        public readonly SamplerState PointWrap;

        /// <summary>
        /// Point filtering with texture coordinate clamping.
        /// </summary>
        public readonly SamplerState PointClamp;

        /// <summary>
        /// Point filtering with texture coordinate mirroring.
        /// </summary>
        public readonly SamplerState PointMirror;

        /// <summary>
        /// Linear filtering with texture coordinate wrapping.
        /// </summary>
        public readonly SamplerState LinearWrap;

        /// <summary>
        /// Linear filtering with texture coordinate clamping.
        /// </summary>
        public readonly SamplerState LinearClamp;

        /// <summary>
        /// Linear filtering with texture coordinate mirroring.
        /// </summary>
        public readonly SamplerState LinearMirror;

        /// <summary>
        /// Anisotropic filtering with texture coordinate wrapping.
        /// </summary>
        public readonly SamplerState AnisotropicWrap;

        /// <summary>
        /// Anisotropic filtering with texture coordinate clamping.
        /// </summary>
        public readonly SamplerState AnisotropicClamp;

        /// <summary>
        /// Anisotropic filtering with texture coordinate mirroring.
        /// </summary>
        public readonly SamplerState AnisotropicMirror;

        /// <summary>
        /// Initializes a new instance of the <see cref="SamplerStateCollection" /> class.
        /// </summary>
        /// <param name="device">The device.</param>
        public SamplerStateCollection(Device device)
        {
            PointWrap = new SamplerState(device, new SamplerStateDescription(){Filter = Filter.MinMagMipPoint, AddressU = TextureAddressMode.Wrap, AddressV = TextureAddressMode.Wrap, AddressW = TextureAddressMode.Wrap});
            PointClamp = new SamplerState(device, new SamplerStateDescription(){Filter = Filter.MinMagMipPoint, AddressU = TextureAddressMode.Clamp, AddressV = TextureAddressMode.Clamp, AddressW = TextureAddressMode.Clamp});
            PointMirror = new SamplerState(device, new SamplerStateDescription(){Filter = Filter.MinMagMipPoint, AddressU = TextureAddressMode.Mirror, AddressV = TextureAddressMode.Mirror, AddressW = TextureAddressMode.Mirror});
            LinearWrap = new SamplerState(device, new SamplerStateDescription(){Filter = Filter.MinMagMipLinear, AddressU = TextureAddressMode.Wrap, AddressV = TextureAddressMode.Wrap, AddressW = TextureAddressMode.Wrap});
            LinearClamp = new SamplerState(device, new SamplerStateDescription(){Filter = Filter.MinMagMipLinear, AddressU = TextureAddressMode.Clamp, AddressV = TextureAddressMode.Clamp, AddressW = TextureAddressMode.Clamp});
            LinearMirror = new SamplerState(device, new SamplerStateDescription(){Filter = Filter.MinMagMipLinear, AddressU = TextureAddressMode.Mirror, AddressV = TextureAddressMode.Mirror, AddressW = TextureAddressMode.Mirror});
            AnisotropicWrap = new SamplerState(device, new SamplerStateDescription(){Filter = Filter.Anisotropic, AddressU = TextureAddressMode.Wrap, AddressV = TextureAddressMode.Wrap, AddressW = TextureAddressMode.Wrap});
            AnisotropicClamp = new SamplerState(device, new SamplerStateDescription(){Filter = Filter.Anisotropic, AddressU = TextureAddressMode.Clamp, AddressV = TextureAddressMode.Clamp, AddressW = TextureAddressMode.Clamp});
            AnisotropicMirror = new SamplerState(device, new SamplerStateDescription(){Filter = Filter.Anisotropic, AddressU = TextureAddressMode.Mirror, AddressV = TextureAddressMode.Mirror, AddressW = TextureAddressMode.Mirror});
            Default = PointWrap;
        }
    }
}