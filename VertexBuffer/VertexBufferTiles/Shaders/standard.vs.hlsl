#include "math.hlsli"
#include "global.hlsli"

float gold_noise(in VertexShaderInput input, in float2 xy, in float seed)
{
    return lerp(0, 1, input.tile.x / map_size.x);
    return lerp(0, 1, (float)1000 / timing.y);

    return frac(tan(distance(xy * phi(), xy) * seed) * xy.x);
}

////////////////////////////////////////////////////////////////////////////////
// Vertex Shader
////////////////////////////////////////////////////////////////////////////////
PixelShaderInput main(VertexShaderInput input)
{
    input.position.w = 1.0f;

    PixelShaderInput output;
    //output.position = input.position;
    output.position = input.position;
    output.position = mul(output.position, world_view_projection);

    /*
    output.color = float4(
        lerp(0, 1, input.tile.x / map_size.x),
        lerp(0, 1, input.tile.y / map_size.y),
        0,
        1);
        */

    const float blend = timing.w > 0.5 ? 1 - timing.w : timing.w;
    output.color = float4(
        lerp(input.tile.x / map_size.x, input.tile.y / map_size.y, blend),
        lerp(input.tile.y / map_size.y, input.tile.x / map_size.x, blend),
        lerp(input.tile.x / map_size.x, input.tile.x / map_size.x, blend),
        1);
    output.tile = input.tile;
    output.uv0 = input.uv0;
    output.uv1 = input.uv0;

    return output;
}
