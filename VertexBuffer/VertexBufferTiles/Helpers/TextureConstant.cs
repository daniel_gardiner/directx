using System.Runtime.InteropServices;
using SharpDX.Mathematics.Interop;

namespace VertexBufferTiles.Helpers
{
    [StructLayout(LayoutKind.Explicit, Size = 8)]
    public struct TextureConstant
    {
        [FieldOffset(0)]
        public RawVector2 PixelSize;

        [FieldOffset(8)]
        public RawVector2 TexelSize;
    }
}