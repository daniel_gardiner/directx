﻿using System;
using System.Collections.Generic;
using System.Linq;
using FluentAssertions;
using SharpDX;
using SharpDX.Mathematics.Interop;
using VertexBufferTiles;
using VertexBufferTiles.Map;
using VertexBufferTiles.Rendering;
using Xunit;
using Xunit.Abstractions;

namespace UnitTests
{
    public class PickingTests
    {
        private readonly ITestOutputHelper _helper;
        private readonly Camera _camera;
        private readonly int _screenHeight;
        private readonly int _screenWidth;
        private int _cursorX;
        private int _cursorY;
        private Map _map;

        public PickingTests(ITestOutputHelper helper)
        {
            _helper = helper;
            _cursorX = 200;
            _cursorY = 200;
            _screenWidth = 800;
            _screenHeight = 600;
            _camera = new Camera();
            _camera.SetViewport(_screenWidth, _screenHeight);

            _map = new Map(new RawVector3(100, 100, 0), _camera);
        }

        [Fact]
        public void PickTile()
        {
            var worldCursor = GetWorldCursor(_cursorX, _cursorY);

            _helper.WriteLine($"screenSize = {(_screenWidth, _screenHeight)}");
            _helper.WriteLine($"cursor = {(_cursorX, _cursorY)}");
            _helper.WriteLine($"worldCursor = {GetWorldCursor(400, 300)}");
            _helper.WriteLine($"worldCursor = {worldCursor}");
        }

        private Vector3 GetWorldCursor(int cursorX, int cursorY)
        {
            var worldCursor = new Vector3
            {
                X = (2.0f * cursorX / _screenWidth - 1) / _camera.ProjectionMatrix.M11,
                Y = -(2.0f * cursorY / _screenHeight - 1) / _camera.ProjectionMatrix.M22,
                Z = 1.0f
            };
            return worldCursor;
        }
        
        private static Vector3 ToVector3(RawVector3 rawVector3)
        {
            return new(rawVector3.X, rawVector3.Y, rawVector3.Z);
        }

        [Fact]
        public void Unproject()
        {
            var screenSize = new Vector2(555, 555);
            var worldMatrix = Matrix.Scaling(64f, 64f, 1);
            var viewMatrix = Matrix.LookAtLH(Vector3.BackwardLH, Vector3.ForwardLH, Vector3.Up);
            var projectionMatrix = Matrix.OrthoLH(screenSize.X, screenSize.Y, -0.1f, 1000f);
        }
    }
}