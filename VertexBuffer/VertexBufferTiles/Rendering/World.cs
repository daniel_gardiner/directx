using System;
using SharpDX;
using SharpDX.Direct3D;
using SharpDX.Direct3D11;
using SharpDX.DXGI;
using SharpDX.Mathematics.Interop;
using Device = SharpDX.Direct3D11.Device;
using Resource = SharpDX.Direct3D11.Resource;

namespace VertexBufferTiles.Rendering
{ 
    public class DX11 : IDisposable
    {
        private Camera _camera;
        private bool VerticalSyncEnabled { get; set; }
        public int VideoCardMemory { get; private set; }
        public string VideoCardDescription { get; private set; }
        private SwapChain SwapChain { get; set; }
        public Device Device { get; private set; }
        public DeviceContext DeviceContext { get; private set; }
        private RenderTargetView RenderTargetView { get; set; }
        private Texture2D DepthStencilBuffer { get; set; }
        public DepthStencilState DepthStencilState { get; set; }
        private DepthStencilView DepthStencilView { get; set; }
        private RasterizerState RasterState { get; set; }
        public Vector2 ScreenSize { get; set; }

        public bool Initialize(Camera camera, Configuration configuration, IntPtr windowHandle)
        {
            _camera = camera;
            _camera.SetViewport(configuration.Width, configuration.Height);
            ScreenSize = new Vector2(configuration.Width, configuration.Height);
            
            try
            {
                VerticalSyncEnabled = Configuration.VerticalSyncEnabled;
                var factory = new Factory1();
                var adapter = factory.GetAdapter1(0);
                var monitor = adapter.GetOutput(0);
                var modes = monitor.GetDisplayModeList(Format.R8G8B8A8_UNorm, DisplayModeEnumerationFlags.Interlaced);
                var rational = new Rational(0, 1);
                if (VerticalSyncEnabled)
                {
                    foreach (var mode in modes)
                    {
                        if (mode.Width != configuration.Width || mode.Height != configuration.Height) 
                            continue;
                        
                        rational = new Rational(mode.RefreshRate.Numerator, mode.RefreshRate.Denominator);
                        break;
                    }
                }

                var adapterDescription = adapter.Description;
                VideoCardMemory = adapterDescription.DedicatedVideoMemory >> 10 >> 10;
                VideoCardDescription = adapterDescription.Description;
                monitor.Dispose();
                adapter.Dispose();
                factory.Dispose();

                var swapChainDesc = new SwapChainDescription()
                {
                    BufferCount = 1,
                    ModeDescription = new ModeDescription(configuration.Width, configuration.Height, rational, Format.R8G8B8A8_UNorm),
                    Usage = Usage.RenderTargetOutput,
                    OutputHandle = windowHandle,
                    SampleDescription = new SampleDescription(1, 0),
                    IsWindowed = !Configuration.FullScreen,
                    Flags = SwapChainFlags.None,
                    SwapEffect = SwapEffect.Discard
                };
                Device.CreateWithSwapChain(DriverType.Hardware, DeviceCreationFlags.Debug, swapChainDesc, out var device, out var swapChain);
                Device = device;
                SwapChain = swapChain;
                DeviceContext = device.ImmediateContext;

                var backBuffer = Resource.FromSwapChain<Texture2D>(SwapChain, 0);
                RenderTargetView = new RenderTargetView(device, backBuffer);
                backBuffer.Dispose();
                var depthBufferDesc = new Texture2DDescription()
                {
                    Width = configuration.Width,
                    Height = configuration.Height,
                    MipLevels = 1,
                    ArraySize = 1,
                    Format = Format.D24_UNorm_S8_UInt,
                    SampleDescription = new SampleDescription(1, 0),
                    Usage = ResourceUsage.Default,
                    BindFlags = BindFlags.DepthStencil,
                    CpuAccessFlags = CpuAccessFlags.None,
                    OptionFlags = ResourceOptionFlags.None
                };
                DepthStencilBuffer = new Texture2D(device, depthBufferDesc);
                var depthStencilDesc = new DepthStencilStateDescription()
                {
                    IsDepthEnabled = false,
                    DepthWriteMask = DepthWriteMask.All,
                    DepthComparison = Comparison.Less,
                    IsStencilEnabled = false,
                    StencilReadMask = 0xFF,
                    StencilWriteMask = 0xFF,
                    FrontFace = new DepthStencilOperationDescription()
                    {
                        FailOperation = StencilOperation.Keep,
                        DepthFailOperation = StencilOperation.Increment,
                        PassOperation = StencilOperation.Keep,
                        Comparison = Comparison.Always
                    },
                    BackFace = new DepthStencilOperationDescription()
                    {
                        FailOperation = StencilOperation.Keep,
                        DepthFailOperation = StencilOperation.Decrement,
                        PassOperation = StencilOperation.Keep,
                        Comparison = Comparison.Always,
                    }
                };
                DepthStencilState = new DepthStencilState(Device, depthStencilDesc);
                DeviceContext.OutputMerger.SetDepthStencilState(DepthStencilState, 1);
                var depthStencilViewDesc = new DepthStencilViewDescription()
                {
                    Format = Format.D24_UNorm_S8_UInt,
                    Dimension = DepthStencilViewDimension.Texture2D,
                    Texture2D = new DepthStencilViewDescription.Texture2DResource()
                    {
                        MipSlice = 0
                    }
                };
                DepthStencilView = new DepthStencilView(Device, DepthStencilBuffer, depthStencilViewDesc);
                DeviceContext.OutputMerger.SetTargets(DepthStencilView, RenderTargetView);
                var rasterDesc = new RasterizerStateDescription()
                {
                    IsAntialiasedLineEnabled = false,
                    CullMode = CullMode.Back,
                    FillMode = FillMode.Solid,
                    IsMultisampleEnabled = true
                };
                RasterState = new RasterizerState(Device, rasterDesc);
                DeviceContext.Rasterizer.State = RasterState;
                DeviceContext.Rasterizer.SetViewport(0, 0, configuration.Width, configuration.Height);

                return true;
            }
            catch
            {
                return false;
            }
        }

        public void BeginScene()
        {
            DeviceContext.ClearDepthStencilView(DepthStencilView, DepthStencilClearFlags.Depth, 1, 0);
            DeviceContext.ClearRenderTargetView(RenderTargetView, new RawColor4(32 / 256f, 103 / 256f, 178 / 256f, 1));
        }

        public void EndScene()
        {
            if (VerticalSyncEnabled)
                SwapChain.Present(1, PresentFlags.None); // Lock to screen refresh rate.
            else
                SwapChain.Present(0, PresentFlags.None); // Present as fast as possible.
        }

        public void Dispose()
        {
            SwapChain?.SetFullscreenState(false, null);
            RasterState?.Dispose();
            RasterState = null;
            DepthStencilView?.Dispose();
            DepthStencilView = null;
            DepthStencilState?.Dispose();
            DepthStencilState = null;
            DepthStencilBuffer?.Dispose();
            DepthStencilBuffer = null;
            RenderTargetView?.Dispose();
            RenderTargetView = null;
            DeviceContext?.Dispose();
            DeviceContext = null;
            Device?.Dispose();
            Device = null;
            SwapChain?.Dispose();
            SwapChain = null;
        }
    }
}