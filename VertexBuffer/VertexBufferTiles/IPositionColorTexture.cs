﻿using SharpDX.Mathematics.Interop;

namespace VertexBufferTiles
{
    public interface IPositionColorTexture
    {
        RawVector3 Position { get; set; }
        RawVector3 Tile { get; set; }
        RawColor4 Color { get; set; }
        RawVector2 UV0 { get; set; }
        RawVector2 UV1 { get; set; }
    }
}